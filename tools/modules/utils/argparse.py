import argparse


# Prepare arguments
def handler_parse():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help="Commands", dest='option')
    subparsers.required = True

    # Parser for Add
    help_parser = 'RetrieveAll'
    parse_retrieve_all(subparsers.add_parser('retrieveAll', help=help_parser))

    help_parser = 'RetrieveSpecific'
    parse_retrieve_specific(subparsers.add_parser('retrieveSpecific', help=help_parser))

    help_parser = 'Deploy'
    parse_deploy(subparsers.add_parser('deploy', help=help_parser))

    help_parser = 'Retrieve'
    parse_retrieve(subparsers.add_parser('retrieve', help=help_parser))

    args = parser.parse_args()

    return args

def parse_retrieve(parser):
    parser.add_argument('-u', '--username', metavar='username',
                        help="Username to deploy/retrieve", required=True)
    parser.add_argument('-p', '--package', metavar='package',
                        help="Package.xml to use.", required=True)
    parser.add_argument('-pth', '--path', metavar='path',
                        help="Path to build retrieve Package", required=True)


def parse_retrieve_all(parser):
    parser.add_argument('-u', '--username', metavar='username',
                        help="Username to deploy/retrieve", required=True)
    parser.add_argument('-p', '--package', metavar='package',
                        help="Package.xml to use.", required=True)
    parser.add_argument('-md', '--metadata', metavar='metadata',
                        help="Metadata choosed.", required=True)


def parse_retrieve_specific(parser):
    parser.add_argument('-u', '--username', metavar='username',
                        help="Username to deploy/retrieve", required=True)
    parser.add_argument('-p', '--package', metavar='package',
                        help="Package.xml to use.", required=True)
    parser.add_argument('-md', '--metadata', metavar='metadata',
                        help="Metadata choosed.", required=True)
    parser.add_argument('-m', '--member', metavar='member',
                        help="Member metadata", required=True)


def parse_deploy(parser):
    parser.add_argument('-u', '--username', metavar='username',
                        help="Username to deploy/retrieve", required=True)
    parser.add_argument('-p', '--path', metavar='path',
                        help="Path to build Package.", required=True)
