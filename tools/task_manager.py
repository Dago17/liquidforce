import errno
import os
import shutil
from pathlib import Path
import zipfile

from modules.utils.argparse import handler_parse
from modules.utils.init import call_subprocess

CWD = os.getcwd()
ERROR_LINE = '*'*28 + ' ERROR ' + '*'*28


HEADER = ("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n" +
          "<Package xmlns=\"http://soap.sforce.com/2006/04/metadata\">\n")
FOOTER = ("\n<version>43.0</version>\n" +
          "</Package>")

FOLDER_META = {'classes', 'certs', 'components', 'contentassets', 'pages',
               'staticresources', 'triggers'}
MAP_META = {
    # DIR_NAME                               XML_NAME

    'installedPackages':                    'InstalledPackage',
    'labels':                               'CustomLabels',
    'staticresources':                      'StaticResource',
    'scontrols':                            'Scontrol',
    'certs':                                'Certificate',
    'aura':                                 'AuraDefinitionBundle',
    'components':                           'ApexComponent',
    'pages':                                'ApexPage',
    'queues':                               'Queue',
    'CaseSubjectParticles':                 'CaseSubjectParticle',
    'dataSources':                          'ExternalDataSource',
    'namedCredentials':                     'NamedCredential',
    'externalServiceRegistrations':         'ExternalServiceRegistration',
    'roles':                                'Role',
    'groups':                               'Group',
    'globalValueSets':                      'GlobalValueSet',
    'standardValueSets':                    'StandardValueSet',
    'customPermissions':                    'CustomPermission',
    'objects':                              'CustomObject',
    'reportTypes':                          'ReportType',
    'reports':                              'Report',
    'dashboards':                           'Dashboard',
    'analyticSnapshots':                    'AnalyticSnapshot',
    'feedFilters':                          'CustomFeedFilter',
    'layouts':                              'Layout',
    'documents':                            'Document',
    'weblinks':                             'CustomPageWebLink',
    'letterhead':                           'Letterhead',
    'email':                                'EmailTemplate',
    'quickActions':                         'QuickAction',
    'flexipages':                           'FlexiPage',
    'tabs':                                 'CustomTab',
    'customApplicationComponents':          'CustomApplicationComponent',
    'applications':                         'CustomApplication',
    'EmbeddedServiceConfig':                'EmbeddedServiceConfig',
    'EmbeddedServiceBranding':              'EmbeddedServiceBranding',
    'flows':                                'Flow',
    'flowDefinitions':                      'FlowDefinition',
    'eventSubscriptions':                   'EventSubscription',
    'eventDeliveries':                      'EventDelivery',
    'workflows':                            'Workflow',
    'assignmentRules':                      'AssignmentRules',
    'autoResponseRules':                    'AutoResponseRules',
    'escalationRules':                      'EscalationRules',
    'postTemplates':                        'PostTemplate',
    'approvalProcesses':                    'ApprovalProcess',
    'homePageComponents':                   'HomePageComponent',
    'homePageLayouts':                      'HomePageLayout',
    'objectTranslations':                   'CustomObjectTranslation',
    'translations':                         'Translations',
    'globalValueSetTranslations':           'GlobalValueSetTranslation',
    'standardValueSetTranslations':         'StandardValueSetTranslation',
    'classes':                              'ApexClass',
    'triggers':                             'ApexTrigger',
    'testSuites':                           'ApexTestSuite',
    'profiles':                             'Profile',
    'permissionsets':                       'PermissionSet',
    'customMetadata':                       'CustomMetadata',
    'profilePasswordPolicies':              'ProfilePasswordPolicy',
    'profileSessionSettings':               'ProfileSessionSetting',
    'datacategorygroups':                   'DataCategoryGroup',
    'remoteSiteSettings':                   'RemoteSiteSetting',
    'cspTrustedSites':                      'CspTrustedSite',
    'matchingRules':                        'MatchingRules',
    'duplicateRules':                       'DuplicateRule',
    'cleanDataServices':                    'CleanDataService',
    'authproviders':                        'AuthProvider',
    'eclair':                               'EclairGeoData',
    'sites':                                'CustomSite',
    'channelLayouts':                       'ChannelLayout',
    'contentassets':                        'ContentAsset',
    'sharingRules':                         'SharingRules',
    'sharingSets':                          'SharingSet',
    'communities':                          'Community',
    'ChatterExtensions':                    'ChatterExtension',
    'callCenters':                          'CallCenter',
    'connectedApps':                        'ConnectedApp',
    'appMenus':                             'AppMenu',
    'delegateGroups':                       'DelegateGroup',
    'siteDotComSites':                      'SiteDotCom',
    'networks':                             'Network',
    'networkBranding':                      'NetworkBranding',
    'communityThemeDefinitions':            'CommunityThemeDefinition',
    'brandingSets':                         'BrandingSet',
    'communityTemplateDefinitions':         'CommunityTemplateDefinition',
    'managedTopics':                        'ManagedTopics',
    'moderation':                           'KeywordList',
    'userCriteria':                         'UserCriteria',
    'moderation':                           'ModerationRule',
    'samlssoconfigs':                       'SamlSsoConfig',
    'corsWhitelistOrigins':                 'CorsWhitelistOrigin',
    'actionLinkGroupTemplates':             'ActionLinkGroupTemplate',
    'synonymDictionaries':                  'SynonymDictionary',
    'pathAssistants':                       'PathAssistant',
    'LeadConvertSettings':                  'LeadConvertSettings',
    'cachePartitions':                      'PlatformCachePartition',
    'topicsForObjects':                     'TopicsForObjects',
    'settings':                             'Settings'
}


def main():
    args = handler_parse()
    if(args.option == 'retrieveAll'):
        if check_package(args.package):
            if exist(f'./tools/config/{args.package}'):
                os.remove(f'./tools/config/{args.package}')
            create_package('*', args.metadata, args.package)
            msg, statusCode = call_subprocess(f'sfdx force:mdapi:retrieve -r ' +
                                              f'./retrieve -u {args.username} ' +
                                              f'-k ./tools/config/{args.package}', False)
            if statusCode != 0:  # SI EL COMANDO HA FALLADO, MUESTRO EL ERROR Y ACABA
                msg, statusCode = call_subprocess('sfdx force:org:list', False)
                print(f'{ERROR_LINE}\n')
                print(f'Please review that your username are here:\n {msg}')
                print('-'*60 + '\n If not, please login Salesforce and try again.')

            else:  # EL COMANDO HA IDO BIEN, SIGO CON EL PROCESO.
                zipfilePath = (f'{CWD}/retrieve/unpackaged.zip')
                zip = zipfile.ZipFile(zipfilePath)
                zip.extractall('.')
                zip.close()
                if not exist(os.path.join('.', 'src')):
                    os.mkdir(os.path.join('.', 'src'))

                for fldr in os.listdir(os.path.join('.', 'unpackaged')):
                    if not os.path.isdir(os.path.join('.', 'src', fldr)):
                        copy(os.path.join('.', 'unpackaged', fldr),
                             os.path.join('.', 'src', fldr))
                    else:
                        shutil.rmtree(os.path.join('.', 'src', fldr))
                        os.rename(os.path.join('.', 'unpackaged', fldr),
                                  os.path.join('.', 'src', fldr))
                shutil.rmtree(os.path.join('.', 'unpackaged'))

    elif(args.option == 'deploy'):
        create_bundle(args.path)
        msg, statusCode = call_subprocess(f'sfdx force:mdapi:deploy -d ' +
                                          f'deploy -u {args.username} -w 1',
                                          False)
        print(msg)
    elif(args.option == 'retrieve'):
        if check_package(args.package):
            if exist(f'./tools/config/{args.package}'):
                os.remove(f'./tools/config/{args.package}')
            fileName, folder = get_info(args.path)
            try:
                xml_name = MAP_META[folder]
            except KeyError:
                print(
                    f'{ERROR_LINE}\n Please open the tab you want to refresh and then launch the command again. Aborting....')
                exit(1)
            create_package(fileName, xml_name, args.package)
            msg, statusCode = call_subprocess(f'sfdx force:mdapi:retrieve -r ' +
                                              f'./retrieve -u {args.username} ' +
                                              f'-k ./tools/config/{args.package}', False)
            if statusCode != 0:  # SI EL COMANDO HA FALLADO, MUESTRO EL ERROR Y ACABA
                msg, statusCode = call_subprocess('sfdx force:org:list', False)
                print(f'{ERROR_LINE}\n')
                print(f'Please review that your username are here:\n {msg}')
                print('-'*60 + '\n If not, please login Salesforce and try again.')

            else:  # EL COMANDO HA IDO BIEN, SIGO CON EL PROCESO.
                zipfilePath = (f'{CWD}/retrieve/unpackaged.zip')
                zip = zipfile.ZipFile(zipfilePath)
                zip.extractall('.')
                zip.close()
                if not exist(os.path.join('.', 'src')):
                    os.mkdir(os.path.join('.', 'src'))

                for fldr in os.listdir(os.path.join('.', 'unpackaged')):
                    if not os.path.isdir(os.path.join('.', 'src', fldr)): # NO EXISTE LA CARPETA
                        copy(os.path.join('.', 'unpackaged', fldr),
                             os.path.join('.', 'src', fldr))
                    else:
                        # EXISTE LA CARPETA
                        for folder_file in os.listdir(os.path.join('.', 'unpackaged', fldr)): # ITERO DENTRO DE LA CARPETA PARA COPIAR SOLO LO QUE SE SOLICITA
                            if exist(os.path.join('.', 'src', fldr, folder_file)):                            
                                if os.path.isdir(os.path.join('.', 'src', fldr, folder_file)):
                                    shutil.rmtree(os.path.join('.', 'src', fldr, folder_file))
                                else:
                                    os.remove(os.path.join('.', 'src', fldr, folder_file))
                            copy(os.path.join('.', 'unpackaged', fldr, folder_file),
                                 os.path.join('.', 'src', fldr, folder_file))
                shutil.rmtree(os.path.join('.', 'unpackaged'))
    elif(args.option == 'retrieveSpecific'):
        if check_package(args.package):
            if exist(f'./tools/config/{args.package}'):
                os.remove(f'./tools/config/{args.package}')

            fileName = args.member
            xml_name = args.metadata

            create_package(fileName, xml_name, args.package)
            msg, statusCode = call_subprocess(f'sfdx force:mdapi:retrieve -r ' +
                                              f'./retrieve -u {args.username} ' +
                                              f'-k ./tools/config/{args.package}', False)
            if statusCode != 0:  # SI EL COMANDO HA FALLADO, MUESTRO EL ERROR Y ACABA
                msg, statusCode = call_subprocess('sfdx force:org:list', False)
                print(f'{ERROR_LINE}\n')
                print(f'Please review that your username are here:\n {msg}')
                print('-'*60 + '\n If not, please login Salesforce and try again.')

            else:  # EL COMANDO HA IDO BIEN, SIGO CON EL PROCESO.
                zipfilePath = (f'{CWD}/retrieve/unpackaged.zip')
                zip = zipfile.ZipFile(zipfilePath)
                zip.extractall('.')
                zip.close()
                if not exist(os.path.join('.', 'src')):
                    os.mkdir(os.path.join('.', 'src'))

                for fldr in os.listdir(os.path.join('.', 'unpackaged')):
                    if not os.path.isdir(os.path.join('.', 'src', fldr)): # NO EXISTE LA CARPETA
                        copy(os.path.join('.', 'unpackaged', fldr),
                             os.path.join('.', 'src', fldr))
                    else:
                        # EXISTE LA CARPETA
                        for folder_file in os.listdir(os.path.join('.', 'unpackaged', fldr)): # ITERO DENTRO DE LA CARPETA PARA COPIAR SOLO LO QUE SE SOLICITA
                            if exist(os.path.join('.', 'src', fldr, folder_file)):
                                if os.path.isdir(os.path.join('.', 'src', fldr, folder_file)):
                                    shutil.rmtree(os.path.join('.', 'src', fldr, folder_file))
                                else:
                                    os.remove(os.path.join('.', 'src', fldr, folder_file))
                            copy(os.path.join('.', 'unpackaged', fldr, folder_file),
                                 os.path.join('.', 'src', fldr, folder_file))
                shutil.rmtree(os.path.join('.', 'unpackaged'))

    print('Finish')


def check_package(name):
    if name.endswith('.xml'):
        return True
    else:
        print(f'{ERROR_LINE}\n {name} did not got the correct extension (.xml)')
        exit(0)


def get_info(filename_path, info_type='folder_basename'):
    if 'folder_basename' == info_type:
        baseName = os.path.splitext(os.path.basename(filename_path))[0]
        parent_path = Path(filename_path).parent
        if 'aura' in parent_path.parts:
            return parent_path.parts[len(parent_path.parts)-1], 'aura'
        else:
            return baseName, parent_path.parts[len(parent_path.parts)-1]
    elif 'filename' == info_type:
        filname = os.path.basename(filename_path)
        return filname


def create_bundle(path):
    fileName, folder = get_info(path)
    try:
        xml_name = MAP_META[folder]
    except Exception:
        print(f'{ERROR_LINE}\n' +
              'Please check that you are placed inside src folder,' +
              ' on the tab with the file you want to upload')
        exit(1)
    create_package(fileName, xml_name)
    create_deploy_folder(get_info(path, 'filename'), folder, path)


def create_package(fileName, xml_name, packageName='deployPackage.xml'):
    if fileName.endswith('-meta'):
        fileName = os.path.splitext(fileName)[0]
    body = f'<types>\n\t<members>{fileName}</members>\n\t<name>{xml_name}</name>\n</types>'
    if os.path.exists(f'./tools/config/{packageName}'):
        print('INFO: Generating a new Package.xml')
        os.remove(f'./tools/config/{packageName}')

    with open(f'{CWD}/tools/config/{packageName}', 'w+') as package:
        package.write(HEADER)
        package.write(body)
        package.write(FOOTER)


def create_deploy_folder(filename, folder, path):
    if os.path.exists(f'{CWD}/deploy'):
        shutil.rmtree(f'{CWD}/deploy')
    # CREATE DEPLOY FOLDER
    os.mkdir(f'{CWD}/deploy')
    os.mkdir(f'{CWD}/deploy/{folder}')
    # COPY PACKAGE AND GENERATE METADATA FOlDER //CHECK IF FOLDER NEEDS META.XML
    if 'aura' == folder:
        handle_aura(path)
    else:
        if folder in FOLDER_META:
            if not filename.endswith('-meta.xml'):
                copy(path, f'{CWD}/deploy/{folder}/{filename}')
                copy(f'{path}-meta.xml',
                     f'{CWD}/deploy/{folder}/{filename}-meta.xml')
            else:
                # COPIO META.XML
                copy(path, f'{CWD}/deploy/{folder}/{filename}')
                filenameAux = filename.replace('-meta.xml', '')  # CORRIJO RUTA
                cwdAux = f'{(Path(path).parent)}/{filenameAux}'
                # COPIO FILE
                copy(f'{cwdAux}', f'{CWD}/deploy/{folder}/{filenameAux}')
        # LA CARPETA NO CONTIENE META ASI QUE COPIO SOLO EL FICHERO NORMAL
        else:
            copy(path, f'{CWD}/deploy/{folder}/{filename}')
    # SE COPIA EL PACKAGE.XML
    copy(f'{CWD}/tools/config/deployPackage.xml', f'{CWD}/deploy/package.xml')


def handle_aura(path):
    aura_folder = get_parent_folder(path)
    aura_path = str(get_parent_path(path))
    copy(aura_path, f'{CWD}/deploy/aura/{aura_folder}')


def get_parent_folder(path):
    return Path(path).parent.parts[len(Path(path).parent.parts)-1]


def get_parent_path(path):
    return Path(path).parent


def exist(path):
    if os.path.exists(path):
        return True
    else:
        return False


def copy(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print(f"Directory not copied. Error: {e}")


main()
