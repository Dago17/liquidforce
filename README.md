
# LiquidForce Tools

# Español

## Requerimientos

- LiquidForce ejecuta comandos de [Salesforce DX](https://developer.salesforce.com/tools/sfdxcli) y necesitas instalarlo.
- Instalar [Python 3.6+](https://www.python.org/downloads/) y seleccionar la opción de añadirlo al PATH o añadir manualmente después.
- Tener Python 3.6+ como interprete en el Visual Studio Code. (``Ctrl/Cmd+Shift+p``)

![Python interpreter](https://i.imgur.com/H33glFx.png)

- [Git](https://git-scm.com/downloads).
- Añadir al PATH el comando code de Visual Studio Code para **``MacOS``**. Presionar ``Cmd+Shift+p`` y escribir ``Shell Command: Install 'code' command in Path``.

![Add code to PATH](https://i.imgur.com/Z9eCwlo.jpg)

## Instalación

Una vez instalada la extension ejecutar comando de vscode ``Ctrl/Cmd+Shift+p``.  Aparecerá la consola de vscode donde hay que escribir ``>Download LiquidForce Tools`` para ejecutar la extension, que descargará una carpeta llamada _liquidforce_ con las herramientas necesarias para trabajar.

![Download LiquidForce](https://media.giphy.com/media/1gVnsJeUw5hImdSe5o/giphy.gif)

## Uso

Para utilizar la extension tendremos que realizar los siguientes pasos:

### Login en Salesforce

Primero tendremos que iniciar la sesion con el usuario con el que vayamos a utilizar, hay dos opciones de login **_test_** o **_login_** y abrira el navegador por defecto para identificarse.

- ``test.salesforce.com``
- ``login.salesforce.com``

#### (Opcional) Usuario por defecto

Para cambiar el usuario por defecto entrar en la carpeta ``.vscode``, ir al archivo ``tasks.json`` y modificar el ``input``:

```json

{
    "id": "Username",
    "description": "Username work with.",
    "default": "<USUARIO>",
    "type": "promptString"
}

```

### Retrieve all o Retrieve specific component

**Comando necesario para empezar a trabajar.**

- #### All

Pedira un usuario con el que trabajar, el nombre del packete que va a generar, por defecto es ``package.xml``, y el tipo de metada que queremos hacer el retrieve.
Cuando finalice genera una carpeta con el nombre de ``src`` y dentro otra con el tipo metadata que hayamos pedido y todos sus metadatos.

- #### Specific component

Pedira un usuario con el que trabajar, el nombre del packete que va a generar, por defecto es ``package.xml``, el tipo de metada que queremos hacer el retrieve y el metadato especifico que queremos.
Cuando finalice genera una carpeta con el nombre de ``src`` y dentro otra con el tipo metadata que hayamos pedido y el metadato seleccionado.

### Retrieve

Para ejecutar este comando **es necesario tener el fichero de componente abierto** en el visual studio code y vuelve a traerte todo el metadato que se encuentra en salecforce ***sobreescribiendo*** el fichero actual.

### Deploy

Para ejecutar este comando **es necesario tener el fichero de componente abierto** en el visual studio code y realizara el deploy de ese metadato.

# English

## Requirements

- LiquidForce use [Salesforce DX](https://developer.salesforce.com/tools/sfdxcli) commands, and you need to install it.
- Install [Python 3.6+](https://www.python.org/downloads/) and select the option to add to the PATH or add manually after.
- Have Python 3.6+ in the Visual Studio Code interpreter. (``Ctrl/Cmd+Shift+p``)

![Python interpreter](https://i.imgur.com/H33glFx.png)

- [Git](https://git-scm.com/downloads).
- Add code a Visual Studio Code command to the PATH for MacOS. Enter ``Cmd+Shift+p`` and write ``Shell Command: Install 'code' command in Path``.

![Add code to PATH](https://i.imgur.com/Z9eCwlo.jpg)

## Install

Once the extension is installed, execute the vscode command ``Ctrl / Cmd + Shift + p``. The vscode console will appear then you have to type ``>Download LiquidForce Tools`` to run the extension, which will download a folder called _liquidforce_ with the necessary tools for works.

![Download LiquidForce](https://media.giphy.com/media/1gVnsJeUw5hImdSe5o/giphy.gif)

 To start working you have to enter the folder downloaded as your workspace.

## Use

To use the extension we will have to perform the following steps:

### Salesforce login

First we will have to start the session with the user we are going to use, there are two login options **_test_** or **_ login_** and it will open the default browser to login in it.

- ``test.salesforce.com``
- ``login.salesforce.com``

#### (Optional) Default user

For change the default username go to ``.vscode``, directory ``tasks.json`` and modify the ``inputs``:

```json

{
    "id": "Username",
    "description": "Username work with.",
    "default": "<USERNAME>",
    "type": "promptString"
}

```

### Retrieve all or Retrieve specific component

**Command needed to start working.**

- #### All

It will ask for a user to work with, the name of the packet that will generate, by default it is ``package.xml``, and the type of metada you want to retrieve.
When it finishes it generates a folder called ``src`` and another in it with the metadata type that we have requested and all its metadata.

- #### Specific component

It will ask for a user to work with, the name of the packet that will generate, by default it is ``package.xml``, the type of metada you want to retrieve, and the specific metadata we want.
When it finishes it generates a folder called ``src`` and another in it with the metadata type that we have requested and the specific metadata.

### Retrieve

To execute this command **it is necessary to have the component file open** in the visual studio code and bring back all the metadata found in salecforce ***overwriting*** the current file.

### Deploy

To execute this command **it is necessary to have the component file open** in the visual studio code and perform the deployment of that metadata.